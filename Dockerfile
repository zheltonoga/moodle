FROM php:5.6-apache
RUN apt-get update
RUN export DEBIAN_FRONTEND=noninteractive && apt-get -o Dpkg::Options::="--force-confnew" install -y apache2 php5  php5-mysql libapache2-mod-php5 php5-gd php5-curl php5-xmlrpc php-pclzip php5-intl mc vim sendmail ghostscript
ADD php_conf/php.ini /usr/local/etc/php/conf.d/
ADD docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
RUN mkdir /var/www/moodledata
RUN chown www-data:www-data /var/www/moodledata
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["apache2-foreground"]
